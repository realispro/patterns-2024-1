package lab.patterns.gof.behavioral.mediator.chat;


public class LaunchChat {

    public static void main(String[] args) {
        User user1 = new User("user1");
        User user2 = new User("user2");
        User user3 = new User("user3");

        user1.send("Hey man!", user2);
        user1.send("Hey man!", user3);
    }
}
