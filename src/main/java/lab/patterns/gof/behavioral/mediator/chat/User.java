package lab.patterns.gof.behavioral.mediator.chat;


public class User {

    private final String userName;

    public User(String userName) {
        this.userName = userName;
    }


    public void send(String message, User cp){
        cp.receive(message);
    }

    public void receive(String message){
        System.out.println("someone send a letter: " + message);
    }
}
