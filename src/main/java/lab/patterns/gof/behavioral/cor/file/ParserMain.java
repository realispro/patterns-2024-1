package lab.patterns.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;

public class ParserMain {

    public static void main(String[] args) throws IOException {
        System.out.println("let's parse a file");

        File file = new File("./src/main/resources/cor/cor.txt");
        TxtParser parser = new TxtParser();
        parser.parse(file);
    }



}
