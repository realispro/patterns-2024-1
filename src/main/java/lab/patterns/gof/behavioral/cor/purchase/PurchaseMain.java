package lab.patterns.gof.behavioral.cor.purchase;

public class PurchaseMain {

    public static void main(String[] args) {
        System.out.println("let's process a purchase");

        Purchase car = new Purchase(243162361, "samochod dla sprzedawcy");
        Purchase paper = new Purchase(40, "papier do drukarki");
        Purchase laptop = new Purchase(8000, "laptop");

        Approver approver = getApprover();
        approver.approve(car);
        approver.approve(paper);
        approver.approve(laptop);

        System.out.println("car = " + car);
        System.out.println("paper = " + paper);
        System.out.println("laptop = " + laptop);
    }


    private static Approver getApprover(){
        Director director = new Director("inz. Karwowski");
        return director;
    }
}
