package lab.patterns.gof.behavioral.cor.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TxtParser {

    public void parse(File file) throws IOException {

        Files
                .lines(Paths.get(file.getAbsolutePath()))
                .forEach(System.out::println);
    }
}
