package lab.patterns.gof.behavioral.cor.purchase;

public abstract class Approver {


    private String name;


    public Approver(String name){
        this.name = name;
    }

    public void approve(Purchase p){
        System.out.println("Przyjęte do realizacji.");
        p.approve(name);
    }


    public String getName() {
        return name;
    }


}
