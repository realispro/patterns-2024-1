package lab.patterns.gof.behavioral.templatemethod.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public abstract class CipherStore extends SafeStore {

    protected abstract byte[] getKey();

    protected abstract String getAlgorithm();

    protected byte[] encrypt(String s) throws Exception {
        Key key = new SecretKeySpec(getKey(), getAlgorithm());
        Cipher c = Cipher.getInstance(getAlgorithm());
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(s.getBytes());
    }
}
