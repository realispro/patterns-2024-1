package lab.patterns.gof.behavioral.observer.press;

public class NewsMain {

    public static void main(String[] args) {

        PressOffice po = new PressOffice();
        po.dayWork();

    }
}
