package lab.patterns.gof.behavioral.observer.followers;

public class FollowersMain {

    public static void main(String[] args) {

        Profile profile = new Profile();

        profile.publish("totally unimportant message 1");
        profile.publish("totally unimportant message 2");
    }
}
