package lab.patterns.gof.behavioral.observer.followers;

public class Profile {



    public void publish(String message) {
        Post p = new Post(message);
        System.out.println("post generated: " + p.getMessage());
        // TODO notify
    }



}
