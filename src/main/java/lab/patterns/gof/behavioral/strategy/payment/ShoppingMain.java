package lab.patterns.gof.behavioral.strategy.payment;

public class ShoppingMain {

    public static void main(String[] args) {
        System.out.println("let's buy smth!");

        Shopping s = new Shopping();

        s.addItem(new Item("mleko", 3, 2.2));
        s.addItem(new Item("Ipa", 24, 3.5));
        s.addItem(new Item("paluszki", 2, 1.79));

        s.pay(
                new Paypal("johny", "bravo"),
                new CreditCard("987654", 456));
    }
}