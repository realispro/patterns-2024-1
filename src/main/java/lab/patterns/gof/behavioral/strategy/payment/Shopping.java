package lab.patterns.gof.behavioral.strategy.payment;

import java.util.ArrayList;
import java.util.List;


public class Shopping {

    private List<Item> items = new ArrayList<Item>();

    public void addItem(Item i){
        items.add(i);
    }



    private double calcPrice(){
        double price = 0;
        for (Item i : items){
            price+=(i.getPrice()*i.getAmount());
        }
        return price;
    }

    public void pay(Paypal p, CreditCard cc){

        if(p!=null){
            p.chargePaypal(calcPrice());
        } else if(cc!=null){
            cc.chargeCard(calcPrice());
        } else {
            throw new RuntimeException("no payment method available");
        }
    }

}
