package lab.patterns.gof.behavioral.strategy.payment;

public class CreditCard {


    private String number;

    private int ccv;


    public CreditCard() {
    }

    public CreditCard(String number, int ccv) {
        this.number = number;
        this.ccv = ccv;
    }

   public void chargeCard(double value) {
        System.out.println("charging card " + number);
    }
}
