package lab.patterns.gof.structural.flyweight.wallet;

public enum Currency {

    PLN,
    EURO,
    DOLLAR
}
