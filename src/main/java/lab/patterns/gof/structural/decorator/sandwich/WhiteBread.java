package lab.patterns.gof.structural.decorator.sandwich;

public class WhiteBread implements Sandwich{
    @Override
    public String parts() {
        return "white bread";
    }
}
