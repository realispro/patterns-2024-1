package lab.patterns.gof.structural.decorator.sandwich;

public interface Sandwich {

    String parts();
}
