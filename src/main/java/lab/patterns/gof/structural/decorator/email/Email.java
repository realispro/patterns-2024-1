package lab.patterns.gof.structural.decorator.email;


public interface Email {

    String getTitle();

    String getContent();
}
