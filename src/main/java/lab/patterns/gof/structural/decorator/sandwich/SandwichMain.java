package lab.patterns.gof.structural.decorator.sandwich;

public class SandwichMain {

    public static void main(String[] args) {
        System.out.println("let's eat some sandwich");

        Sandwich sandwich = new WhiteBread();

        System.out.println("eating " + sandwich.parts());
    }
}
