package lab.patterns.gof.structural.decorator.email;

public class EmailMain {


    public static void main(String[] args) {
        System.out.println("let's send some email");

        Email email = new EmailMessage("Birthday party invitation", "Let's celebrate my birthday tomorrow :)");

        System.out.println("sending email: title=[" + email.getTitle() + "], content=[" + email.getContent() + "]");

    }

}
