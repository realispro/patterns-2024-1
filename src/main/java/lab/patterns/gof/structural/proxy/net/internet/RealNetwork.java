package lab.patterns.gof.structural.proxy.net.internet;


import lab.patterns.gof.structural.proxy.net.Internet;

public class RealNetwork implements Internet {

    public RealNetwork(){}

    @Override
    public void connectTo(String address) {
        System.out.println("connecting to " + address);
    }
}
