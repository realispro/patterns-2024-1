package lab.patterns.gof.structural.proxy.net;


public class InternetMain {

    public static void main(String[] args) {
        System.out.println("lets surf!");

        Internet internet = null;

        internet.connectTo("wp.pl");
        internet.connectTo("facebook.com/events");

    }
}
