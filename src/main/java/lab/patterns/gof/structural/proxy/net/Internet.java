package lab.patterns.gof.structural.proxy.net;

public interface Internet {


    void connectTo(String address);
}
