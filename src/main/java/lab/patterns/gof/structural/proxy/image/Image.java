package lab.patterns.gof.structural.proxy.image;

public interface Image {

    byte[] getData();

}
