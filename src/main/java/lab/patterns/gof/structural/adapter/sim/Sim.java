package lab.patterns.gof.structural.adapter.sim;

public interface Sim {

    String getId();

}
