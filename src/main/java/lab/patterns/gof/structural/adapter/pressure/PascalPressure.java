package lab.patterns.gof.structural.adapter.pressure;

public interface PascalPressure {

    float getPressure();
}
