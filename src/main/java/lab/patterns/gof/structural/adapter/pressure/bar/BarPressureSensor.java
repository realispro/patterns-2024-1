package lab.patterns.gof.structural.adapter.pressure.bar;

import java.util.Random;

public class BarPressureSensor {

    public float getPressure(){
        return new Random().nextFloat();
    }

}
