package lab.patterns.gof.structural.adapter.sim;

public class MiniSimCard {

    private byte[] id;

    public MiniSimCard(String id) {
        this.id = id.getBytes();
    }

    public byte[] getId() {
        return id;
    }
}
