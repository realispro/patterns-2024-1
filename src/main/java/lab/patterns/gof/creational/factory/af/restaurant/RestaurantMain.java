package lab.patterns.gof.creational.factory.af.restaurant;

public class RestaurantMain {

    public static void main(String[] args) {

        Restaurant restaurant = null; //

        MainDish mainDish = restaurant.getMainDish();
        mainDish.eat();

        Dessert dessert = restaurant.getDessert();
        dessert.enjoy();
    }
}
