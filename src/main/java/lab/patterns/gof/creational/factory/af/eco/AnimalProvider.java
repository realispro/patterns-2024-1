package lab.patterns.gof.creational.factory.af.eco;


public interface AnimalProvider {

    Bird provideBird();

    Fish provideFish();

    Mammal provideMammal();


}
