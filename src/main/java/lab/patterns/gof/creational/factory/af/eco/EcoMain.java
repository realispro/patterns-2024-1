package lab.patterns.gof.creational.factory.af.eco;

public class EcoMain {

    public static void main(String[] args) {

        AnimalProvider ap = null;

        Bird bird = ap.provideBird();
        bird.fly();

        Fish fish = ap.provideFish();
        fish.swim();

        Mammal mammal = ap.provideMammal();
        mammal.move();

    }
}
