package lab.patterns.gof.creational.factory.af.eco;

public interface Mammal {

    void move();
}
