package lab.patterns.gof.creational.factory.af.eco;

public interface Fish {

    void swim();

}
