package lab.patterns.gof.creational.factory.method;

public class ZywiecIpa implements Beer {
    @Override
    public String getName() {
        return "ipa beer";
    }
    @Override
    public float getVoltage() {
        return 4;
    }
}
