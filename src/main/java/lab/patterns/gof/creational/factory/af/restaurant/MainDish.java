package lab.patterns.gof.creational.factory.af.restaurant;

public interface MainDish {

    void eat();
}
