package lab.patterns.gof.creational.factory.af.restaurant;

public interface Appetizer {

    void taste();
}
