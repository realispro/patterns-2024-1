package lab.patterns.gof.creational.factory.af.restaurant;

public enum RestaurantType {

    POLISH,
    ITALIAN
}
