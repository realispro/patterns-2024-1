package lab.patterns.gof.creational.factory.af.restaurant;

public interface Dessert {

    void enjoy();
}
