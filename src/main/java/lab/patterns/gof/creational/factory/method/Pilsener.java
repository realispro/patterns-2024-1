package lab.patterns.gof.creational.factory.method;

public class Pilsener implements Beer {
    @Override
    public String getName() {
        return "plis beer";
    }
    @Override
    public float getVoltage() {
        return 5;
    }
}
