package lab.patterns.gof.creational.factory.af.eco;

public interface Bird {

    void fly();
}
