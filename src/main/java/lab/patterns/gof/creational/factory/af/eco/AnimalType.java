package lab.patterns.gof.creational.factory.af.eco;

public enum AnimalType {

    PREHISTORIC,
    PREDATOR
}
