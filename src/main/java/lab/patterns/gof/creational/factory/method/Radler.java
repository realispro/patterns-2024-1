package lab.patterns.gof.creational.factory.method;

public class Radler implements Beer {
    @Override
    public String getName() {
        return "fruity beer";
    }
    @Override
    public float getVoltage() {
        return 2;
    }
}
