package lab.patterns.gof.creational.factory.method;

public interface Beer {

    float getVoltage();

    String getName();

}
