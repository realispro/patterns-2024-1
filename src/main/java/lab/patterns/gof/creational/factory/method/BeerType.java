package lab.patterns.gof.creational.factory.method;

public enum BeerType {
    PILS,
    IPA,
    FRUITY
}
