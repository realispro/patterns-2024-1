package lab.patterns.gof.creational.singleton;

public class DisplayMain {

    public static void main(String[] args) {

        Display display1 = new Display();
        Display display2 = new Display();

        display1.show("text from display1");
        display2.show("text from display2");
        System.out.println("same? " + (display1==display2));

    }
}
