package lab.patterns.gof.creational.singleton;

public class Display {

    public void show(String text){
        System.out.println("[" + text + "]");
    }

}
