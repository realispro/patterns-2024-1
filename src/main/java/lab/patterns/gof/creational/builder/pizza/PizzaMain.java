package lab.patterns.gof.creational.builder.pizza;

public class PizzaMain {

    public static void main(String[] args) {
        System.out.println("let's eat some pizza!");

        Pizza pizza = new PizzaBuilder()
                .withDough("grube")
                .withTopping("ananas")
                .withTopping("tuna")
                .withSause("czosokowy")
                .bake();

                /*new Pizza();
        pizza.setDough("cienkie");
        pizza.addTopping("salami");
        pizza.addTopping("pomidor");
        pizza.addTopping("ser");
        pizza.setSause("czosnkowy");*/

        System.out.println("eating pizza " + pizza);
    }

}
