package lab.patterns.gof.creational.builder.pizza;

import java.util.List;

public class PizzaBuilder {

    private Pizza pizza = new Pizza();

    private String dough;
    private List<String> toppings;
    private String sause;

    public void setDough(String dough){
        pizza.setDough(dough);
    }

    public PizzaBuilder withDough(String dough){
        pizza.setDough(dough);
        return this;
    }

    public PizzaBuilder withTopping(String topping){
        pizza.addTopping(topping);
        return this;
    }

    public PizzaBuilder withSause(String sause){
        pizza.setSause(sause);
        return this;
    }

    public Pizza bake(){
        if(pizza.getDough()==null){
            throw new IllegalStateException("missing dough");
        }
        return pizza;
    }

}
