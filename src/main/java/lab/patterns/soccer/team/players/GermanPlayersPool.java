package lab.patterns.soccer.team.players;

import lab.patterns.soccer.team.Player;

import java.util.Arrays;
import java.util.List;

public class GermanPlayersPool implements PlayersPool {

    private static GermanPlayersPool instance = new GermanPlayersPool();

    private GermanPlayersPool(){}

    public static GermanPlayersPool getInstance() {
        return instance;
    }


    public static List<Player> goalKeepers = Arrays.asList(
            new Player("Manuel", "Neuer", 1)
    );

    public static List<Player> defenders = Arrays.asList(
            new Player("Mats", "Hummels", 120),
            new Player("Jonas", "Hector", 19),
            new Player("Jerome", "Boateng", 19),
            new Player("Emre", "Can", 19)
    );

    public static List<Player> midfields = Arrays.asList(
            new Player("Toni", "Kroos", 120),
            new Player("X", "Goetze", 122),
            new Player("X", "Szweinsteinger", 88),
            new Player("Mesut", "Ozil", 88),
            new Player("Marco", "Reus", 88),
            new Player("Sami", "Khedira", 19)
    );

    static List<Player> attackers = Arrays.asList(
            new Player("X", "Ballack", 2),
            new Player("Tomas", "Mueller", 12),
            new Player("Lukas", "Podolski", 19),
            new Player("Mario", "Gomez", 19)
    );


    @Override
    public List<Player> getGoalKeepers() {
        return goalKeepers;
    }

    @Override
    public List<Player> getDefenders() {
        return defenders;
    }

    @Override
    public List<Player> getMidfields() {
        return midfields;
    }

    @Override
    public List<Player> getAttackers() {
        return attackers;
    }
}
