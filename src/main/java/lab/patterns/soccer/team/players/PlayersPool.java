package lab.patterns.soccer.team.players;

import lab.patterns.soccer.team.Player;

import java.util.List;

public interface PlayersPool {

    List<Player> getGoalKeepers();

    List<Player> getDefenders();

    List<Player> getMidfields();

    List<Player> getAttackers();


}
