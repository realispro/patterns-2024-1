package lab.patterns.soccer.match;

import lab.patterns.soccer.team.Team;

import java.util.HashMap;
import java.util.Map;

public class Match {

    private Team team1;

    private Team team2;

    private Map<String, Integer> result = new HashMap<>();


    public Match(Team team1, Team team2) {
        this.team1 = team1;
        this.team2 = team2;
        result.put(team1.getName(), 0);
        result.put(team2.getName(), 0);

        introduction();
    }

    public void introduction(){
        System.out.println("[ Presenting teams ]\n");
        System.out.println(team1.getName() + " vs " + team2.getName() + "\n");
        System.out.println(team1.info());
        System.out.println(team2.info());
    }


    public void score(Team t){
        Integer r = result.get(t.getName());
        result.put(t.getName(), ++r);
    }


    public void play(){

        // TODO emulate game

    }




    public void showResult() {

        int team1result = result.get(team1.getName());
        int team2result = result.get(team2.getName());

        System.out.println("\n" + team1.getName() + " : " + team2.getName());
        System.out.println(team1result + " : " + team2result);

        if(team1result==team2result){
            System.out.println("There was a draw");
        } else {
            Team won = team1result>team2result ? team1 : team2;
            Team lost = team1result>team2result ? team2 : team1;

            System.out.println("And the winner is: \n" + won);
            System.out.println("Sorry, boys: \n" + lost);
        }


    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }





}
