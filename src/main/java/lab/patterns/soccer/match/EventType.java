package lab.patterns.soccer.match;

public enum EventType {
    GOAL,
    YELLOW_CARD,
    RED_CARD
}