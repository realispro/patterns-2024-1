package lab.patterns.soccer;


import lab.patterns.soccer.match.Match;
import lab.patterns.soccer.team.Team;

public class SoccerMain {

    public static void main(String[] args) {

        Team polska = null;

        Team niemcy = null;

        Match match = new Match(polska, niemcy);

        match.play();
        match.showResult();
    }
}
