package lab.solid.employees;

import lab.solid.IssueHandler;

import java.util.Random;

public class Intern implements IssueHandler {

    @Override
    public void handleIssue(String issue, IssueHandler issueHandler2) {
        System.out.println("intern is handling issue " + issue);
        if(new Random().nextBoolean()){
            issueHandler2.handleIssue(issue, null);
        }
    }
}
