package lab.solid.employees;

import lab.solid.Employee;
import lab.solid.IssueHandler;

public class Specialist extends Employee implements IssueHandler {

    public Specialist(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doMagic(){
        System.out.println(getLastName() + " is doing magic");
    }

    @Override
    public void handleIssue(String issue, IssueHandler issueHandler2){
        System.out.println(getLastName() + " is handling issue: " + issue);
    }


}
