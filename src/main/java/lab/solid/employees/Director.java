package lab.solid.employees;

import lab.solid.Employee;
import lab.solid.Management;

public class Director extends Employee implements Management {
    public Director(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }
}
