package lab.solid.employees;

import lab.solid.Employee;

public class Accountant extends Employee {


    public Accountant(String firstName, String lastName, int grade) {
        super(firstName, lastName, grade);
    }

    public void doAccounting(){
        System.out.println("accountant is accounting");
    }


}
