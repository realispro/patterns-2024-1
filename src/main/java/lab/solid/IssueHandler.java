package lab.solid;

@FunctionalInterface
public interface    IssueHandler {

    void handleIssue(String issue, IssueHandler issueHandler2);

}
